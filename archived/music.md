---
sidebar_position: 6
---

# Music
Listen to music with Potato Bot!

## What is it?

The music system allows you to use Potato Bot to listen to music in a voice channel. At the moment, we use YouTube to get our music, but, given how much YouTube hates Discord bots, that could change at anytime. But for right now, that's what this is.

## Important Info
- Music Queues cannot be shared across servers.
- Potato Bot can only play music in one voice channel at a time.


## Command Details
At least one song must be in the queue (or playing) for any of these commands to work, except for `/play`.

All music management controls require the user to:
- Be in the same channel as the bot
- Have the Manage Messages permission

The only exceptions are `/play`, `/queue`, `/nowplaying`, and `/effect`.
### /effect
**Donator only due to high CPU usage**

Depending on the **option** argument, this command applies an effect to the currently playing music.

There are 6 different effects to choose from:
- None (Turns off any effects, if there are any)
- Surround (Headphones needed, bounces the music back and forth between left and right)
- Bass Boost (Amplifies the bass on the music)
- Earrape (Very loud, not recommended for headphones)
- Nightcore (High pitched, sped up)
- Vaporwave (Low pitched, slowed down)

This command must have 1 argument passed. The **option** argument determines what effect should be applied.

### /loop
**Donator only due to high CPU usage**

Depending on the **mode** argument, this command either loops a current song playing or the entire song queue for that server.

This command must have 1 argument passed. The **mode** argument determines whether to loop the queue or just the current song playing.

### /nowplaying

This command embeds the current song that is playing in the server.

This command has no arguments.

### /pause

This command pauses the currently playing song. (See `/resume` for unpausing songs)

This command has no arguments.

### /play

This command searches YouTube for a song that is passed in the **term** argument. The argument can either be a YouTube URL or search term. If it is a search term, Potato Bot will pick the first video that comes up.

> **Note**: Livestreams will not work as they run all the time. If you are a donator, however, you can use livestreams.

This command must have 1 argument passed. The **term** argument is either a search term or YouTube URL.

### /queue

This command embeds the currently playing songs, ordered by what is coming up next, so for example, a queue could look like this:

- HOME - Resonance
- Tobu - Candyland
- Leonz - Among Us Drip

Assuming a song is already playing, the song after the currently playing one would be Resonance, followed by Candyland, and so on. Users with the correct permissions can change the queue. (See `/remove`)

This command has no arguments.

### /remove

This command removes a song from the current server queue.

This command must have 1 argument passed. The **position** argument is the position of the target song to remove. (See `/queue` to see how this works)

### /resume

This command resumes music that is paused. (See `/pause`)

This command has no arguments.

### /skip

This command skips the currently playing song and goes to the next song in the queue, if there are any.

> **Note**: If there are no more songs in the queue when the skip command is ran, the music queue is considered over.

This command has no arguments.

### /stop

This command stops the music queue immediately, regardless of how many more songs are in the queue.

This command has no arguments.

### /volume

This command adjusts how loud music is played through the bot.

The **value** argument, which is what is used to determine how loud the volume is, can go from **1-150**.

This command must have 1 argument passed. The **value** argument is the desired loudness the music player should be at.