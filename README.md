# Overclocked Documentation

This repository contains the code for the OC Development documentation. This is built using the [Docusaurus](https://docusaurus.io) project.

## Contributing

If you wish to contribute to the docs, please navigate to the `docs` folder, then open the folder to the page you wish to contribute to, and make your edits. (You can use GitLab's editor or clone the repository to your local machine) Pages are all markdown, so please make sure you know how to use markdown before doing anything.

Once you've made your edits, commit it and open a merge request. We will then decide whether or not to merge your changes into our docs.

**Please also keep in mind we want to keep things consistent, so please follow the style in the other categories.**

## Installation

If you wish to clone the repository to your local machine and run it there, please make sure you have done the following:

- Installed NodeJS v16 or higher
- Installed NPM v8 (i believe) or higher

Then, **globally** install the create-docusaurus package, then, go to the folder in which you have cloned the repository, and install the needed dependencies for the repository itself using `npm i`.

Once you have done that, you can then run `npm run start` to start the server. Depending on your OS, the browser should auto launch. If it doesn't, navigate to http://localhost:3000. You don't need to restart the server for each edit you make, as it will reload the page upon any file edits.