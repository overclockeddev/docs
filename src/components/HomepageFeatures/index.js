import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Get Useful Info',
    Svg: 'text',
    description: (
      <>
        This site was made to get info about how to use Overclocked bots and its commands.
      </>
    ),
  },
  {
    title: 'Constantly Growing',
    Svg: 'text',
    description: (
      <>
        This site is still under-construction, and we are adding more documentation in the future.
        If you wish to contribute, hit the Repository button in the top!
      </>
    ),
  },
  {
    title: 'Powered by OSS',
    Svg: 'text',
    description: (
      <>
        This page is made possible by the Docusaurus project.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
