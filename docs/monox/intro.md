---
sidebar_position: 1
---

# Introduction
Get to know the Monoxide bot.

Monoxide is a whitelist-only moderation bot that aims to keep everything free to use.

**Documentation for Monoxide is coming soon!**

**If you know your way around Monoxide, we encourage you to read the README file at the root of this repository to contribute to our docs.**