---
sidebar_position: 2
---

# Privacy Policy
Last Modified December 19th, 2023

## Introduction
Whenever you utilize Overclocked services or Discord bots, some of your data is sent to us in order for these services to function. You will find out what we collect, how we use it, and how you can opt out.

## General Data Collected
If you are using an Overclocked Discord bot, the following data is stored **whenever a command is ran**:

- Your Discord Username and ID
- The server's name and ID (in which the command was ran in)
- What command you ran, along with any arguments

When you invite an Overclocked Discord bot, the following data is stored in a Discord channel:

- The server's ID
- The server owner's username and ID
- How many members the server has

None of the data we collect about command usage (except when you last ran one) is stored off Discord.

This data is used to keep track of what commands are being ran, who is running them and where, in the event we need to terminate an account from our services due to inactivity or a breach of our policies. We also may, but not always, use the data for analytical purposes. All the data we store weill NEVER be sold or given to third-parties or advertisers.

## Data Collected (Potato Bot)

Potato Bot will collect some information and store it **outside of Discord**. The only identifying information we keep outside of Discord is your Discord User ID. The rest of the data associated with your profile is currency, preference, and activity data.

The data is used, and necessary, to provide Potato Bot to the user. Currency data is available to the general public through certain commands on the bot.

## Data Collected (Potato Bot - Chat)

Potato Bot utilizes the OpenAI API to serve you its chat function. You may view their privacy policy at https://openai.com/policies/privacy-policy.

Overclocked stores the following data **within Discord** when you invoke a chat:

- Your username and ID
- Your message and Potato Bot's response
- The server's name and ID in which you invoked the chat

This data is used for moderation and quality purposes.

## Data Collected (Monoxide)

Monoxide will collect some information and store it **outside of Discord**. The only identifying information we keep outside of Discord is each Discord Server ID, which we will refer to as a "guild profile". The rest of the data associated with the server profile is preferences data.

If users issue a punishment, the following information is stored in the guild profile for the modlog function:

- The victim (offending user's) Discord User ID
- The moderator's (defending user's) Discord User ID
- The reason the moderator assigned to the case
- (If applicable) The duration of the punishment.

Cases can be deleted through a command on the Monoxide bot itself. The data is used, and necessary, to provide Monoxide to each server's users. No data is generally available to the public, but can be obtained through running some commands on the bot itself.

## How to Remove Your Data
If you ever wish to remove data from Overclocked's systems for any reason, you may choose to do so at any time. The process may vary slightly depending on which service you are requesting, but in most cases the process should be similar.

To request data to be removed, email support@overclocked.dev with the following information:

- Your Discord User/Server ID
- The data to be removed (can be specific data or all)
- Proof of ownership of the user account or server (we will not take requests from users who do not own the specified server or user account)

Please note that after you submit your request and we process it, we are not able to recover any of your data. If you wish to use our services again, your data will be remade with what you started out with. If you change your mind after you submit your request, please reply to your email thread ASAP before we process it.

## How to Request Your Data
If you ever wish to request any of your data from Overclocked's systems for any reason, you may choose to do so at any time, The process may vary slightly depending on which service you are requesting, but in most cases the process should be similar.

To request your data, email support@overclocked.dev with the following information:

- Your Discord User/Server ID
- The data to be requested (can be specific data or all)
- Proof of ownership of the user account or server (we will not take requests from users who do not own the specified server or user account)

Please note you can only do this once every 14 days.

## Under 13 Data Collection
Overclocked does not knowingly collect information from anyone under the age of 13. If you believe your child has data stored with us, please follow the process in **How to Remove Your Data**. Please also include that you are a parent/guardian, so that we may restrict them from our services (if applicable) until they are old enough.