---
sidebar_position: 1
---

# Terms of Service
Last Modified December 19th, 2023

> **Disclaimer**: I am not a lawyer, so some things that you would expect to find may be missing. I'll do my best to update it over time to give more accurate information.

## Introduction
Whenever you utilize Overclocked services or Discord bots, you are subject to these Terms and Conditions. We will ask whether or not you agree to these terms prior to your usage of these services. If you do not agree with partial or all information presented here, you are not authorized to use our services and/or Discord bots.

## Licensing
The Overclocked services, unless otherwise stated, is licensed under a proprietary license. Without authorization, you are not allowed to:
- "Reverse-engineer" our services
- Use our assets (assets that are not owned by us but are present in one of our services will be stated somewhere)
- Modify or copy the legitimate source code
- Use our services for monetary gain

## Termination
Overclocked has the right to terminate your access to a service at any given time, for any reason. In addition to this, you have a right to terminate your own data at any given time. Please refer to our Privacy Policy for more details.

Overclocked is not obliged to re-instate terminated accounts.

## Warranty
Overclocked services are provided "as-is", and because of this, no warranty can be provided. We do offer free support through any of our contact methods.

## General ToS Statements
This ToS and Privacy Policy can be updated at any given point, whether individuals are notified ahead of time or not. If you continue to use our services, you are agreeing to the current version of the Terms and Privacy Policy.

If you have questions or concerns with these terms, please email support@overclocked.dev

**Copyright 2023 Overclocked Development**