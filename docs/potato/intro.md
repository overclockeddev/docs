---
sidebar_position: 1
---

# Introduction
Get to know Potato Bot.

Potato Bot is an economy bot based on the P-Coin system. You can play some games with your friends, race to the top of leaderboards, and even talk to Potato Bot himself!

Potato is a slash-only bot, meaning Potato cannot use traditional message based commands.

To use its commands, press the `/` key in the Discord chat. If you do not see Potato's icon in the menu, please check the following:

- The link you invited Potato with said "Create commands in a server"
- Your server does not have too many bots. (Discord likes to hide commands from some bots when too many bots are added)

If that doesn't work, restart your Discord client to clear its cache.

Regardless, you should reinvite Potato Bot using the most **up-to-date** invite link. It can be found [here](https://top.gg/bot/733000527243247678/invite).

## What this doc covers

The documentation covers command info, what the categories are about, as well as more detailed information on how to use the bot. When you're ready, make a selection by using the bar to the left, or, click the **Next** button below this to get started with the first category.