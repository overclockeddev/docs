---
sidebar_position: 6
---

# Fun
Nothing wrong with having a little fun on Potato Bot.

## What is it?
The fun category is just fun commands you can use. They won't affect your P-Coin balances, or do anything. It's just plain fun.

## Command Details

### /8ball

This command lets you ask a yes/no question to get an answer. Inspired by the Magic 8 Ball.

This command has 3 results, each with different messages. Positive (yes), neutral, or negative (no)

> **Note**: Asking discriminitive, offensive, or illegal questions is bannable.

> **Note**: Please do not take anything the Magic 8 Ball says as advice. This is simply for fun.

This command must have 1 argument passed, with 1 optional one. The **question** argument is the question that you want to ask, and the optional **private** argument is to let Potato's responses be only visible to you.

### /chat

This command lets Potato Bot use the power of AI to chat with you personally!

Keep in mind that since responses are generated in real-time, some responses may be inaccurate. Also, we do our best to make sure that Potato can't swear, but sometimes, things slip through. This being the case, we may monitor your conversations with Potato Bot as part of our efforts to keep things safe.

This command must have 1 argument passed, with 1 optional one. The required **prompt** argument is the sentence you want to say to Potato Bot, and the optional **private** argument is to let Potato's responses be only visible to you.

> **Note**: Please don't take anything Potato Bot says as advice. This is simply for fun.

> **Note**: Telling Potato Bot anything (or relating to) discriminitive, offensive, or illegal content is bannable. Also, attempting to bypass the built-in swear filter will result in an account ban.

### /choose

This command lets Potato Bot choose between two options you type in. Potato Bot has several unique responses for revealing his choice.

This command must have 2 arguments passed. The **thing1** and **thing2** arguments are the options to let Potato Bot pick from.

> **Note**: Potato Bot will not mention users (or everyone) with this command.

### /flip

This command has 2 outcomes:
- Heads
- Tails

This command can be used for any purpose, as long as it has 2 options, which is what "flipping a coin" actually means.

This command has no arguments.

### /meme

This command pulls a random meme from the subreddit r/memes and embeds it. There is also a button on the message that a user can click to get another meme.

> **Note**: We are not responsible for the memes posted on that subreddit. Please use this at your own discretion.

This command has no arguments.

### /moonbasetts

This command allows you to use the TTS (text-to-speech) engine from the game "Moonbase Alpha". This works by taking your input, processing it, and then sending a .wav file in chat.

**Make sure Potato Bot has the Attach Files permission!** (You should already have it set up, but we noticed that not all people do this.)

> **Note**: If you are on mobile, you may see that your only option is to download the output file. If this happens, please switch to desktop so that Discord can play it. (We know it's a hassle, Discord doesn't know how to design a mobile app.)

> **Note**: Attempting to use the TTS maliciously will get you banned from Potato Bot.

This command must have 1 argument passed. The **input** argument is the content you wish to have generated.

### /qr

Create a QR code of any text content. People can then scan it with a QR scanning program (like a phone's camera app) to then recieve the text that you passed into the **content** argument.

> **Note**: YOU are responsible for the QR codes generated by you. Attempting to use QR codes maliciously will get you banned from Potato Bot.

This command must have 1 argument passed. The **content** argument is the content that the generated QR code should have.