---
sidebar_position: 4
---

# Messaging

Occasionally, you may get messages from the Potato Bot developers. They're usually delivered in two different ways, and you may read about them below.

## Developer Messages

These are used for less important matters, and will only be delivered to you on your next command run in an ephemeral (hidden) message. They will usually include one of the following:

- News and updates regarding the bot
- Information regarding your account (e.g. any potential rule breakage)

They will be dismissed when you run your command, and they will not be able to be shown again, so pay attention!

## DM Messages

These types of messages get delivered directly to your Direct Messages, and they're only used for **important** matters. They cannot be turned off, but you can disable DMs from Potato Bot. However, we don't recommend doing that as they hold more important information than standard developer messages. They will usually include one of the following:

- Important information regarding your account (e.g. punishments issued)
- Impending updates that affect your account (e.g. data resets)