---
sidebar_position: 5
---

# Economy
Use the P-Coin system to get to the top.

You're probably using Potato Bot for its P-Coin features. We don't blame you. Here's what you need to know.

## What is it?

The economy system is a global system (spanning across all servers) that allows users to have our currency, P-Coin, that can be used on Potato Bot. Being a global system, users can gain and lose P-Coin in all servers the bot is in. Users can even play together using the economy system through some commands.

> Note: There is a 1 billion P-Coin hardlimit on everyone's wallet to keep things fair.

## Vault

P-Coin in your vault cannot be affected unless you manually deposit or withdraw P-Coin from it. (see `/deposit` and `/withdraw`)

Vault space will increase the more you use Potato Bot, starting at 1,000,000 P-Coin space.

All commands that use P-Coin will only use your pocket value, including commands other people use.

## Server Multipliers

Each server can get a specific multiplier that affects P-Coin earned there. However, this is only changable by the bot administrators. You may see if you have a mulitplier active in a server by looking at the embed when gaining P-Coin, it should be a seperate field called **Server Multiplier**, followed by a number showing how much your earnings were multiplied.

## Command Details

**Before we begin, keep in mind we will be using the terms *source* and *target* users.**
- A source user refers to the user who ran a command 
- A target user is a user who the command is ran on (ex. getting robbed)

### /balance

This command checks the balance of either you or another user.

This command shows:
- Pocket value (Availale P-Coin)
- Vault value (Unavailable P-Coin, can't be robbed, see **Vault** section)
- M-Coin invested (Used for stocks, see `/stock`)

If no arguments are passed, the command defaults to your user. To check another user's balance, pass in a different user in the **user** argument.

### /beg

This command "begs" for P-Coin. The possibility of getting P-Coin or not is random, and the amount you will get is random. This command has no arguments. You can't lose P-Coin from this.

### /blackjack

Using the **bet** argument, this command bets an amount of P-Coin, and plays a game of blackjack with the bot itself.

It is recommended that you know how to play blackjack before doing this.

There are 5 possible outcomes, depending on what you do:
- Win the game, you get a fraction of your bet (you will not lose your original bet)
- Lose the game, you lose your bet
- Tie the game, your balance won't be affected
- Run out of time, your balance won't be affected
- Quit the game early, you lose half of your bet

This command must have 1 argument passed. The **bet** argument is how much you want to bet in your game.

### /daily

Every 24 hours, you can run this command to get a bonus depending on what your current P-Coin wallet and vault value is.

The amount gained will not surpass a third of your total P-Coin values.

This command has no arguments.

### /deposit

Using the **amount** argument, this command deposits an amount of P-Coin into your vault. (See **Vault** section)

This command must have 1 argument passed. The **amount** argument is how much you want to deposit into your vault.

### /gamble

Using the **bet** argument, this command gambles a set amount P-Coin to either gain some back, or lose it all.

There are 2 outcomes.
- Gain some of your betted P-Coin back
- Lose all of your bet

This command must have 1 argument passed. The **bet** argument is how much you want to bet your P-Coin.

### /leaderboard

This command displays the users on Potato Bot, ordered by how much P-Coin is in their pocket. This shows 10 users per page, and if no arguments are passed defaults to the first page.

For each user, it shows:
- Their username
- How much their pocket is worth

This command has 1 optional argument: the **page** argument is the page the embed should show, grouped with 10 users per page.

### /minigame

This command will choose from one of several pre-built minigames. The minigames are used to gain P-Coin, and some of them will make you lose P-Coin.

There are currently 2 minigames:
- Quick Time - Press the button when it appears. It might disappear!
- Hidden Button - One of the buttons will win. The others will lose.

This command must have 1 argument passed. The **bet** argument determines how much of the user's P-Coin is going to be lost or gained, depending on the result of the minigame.

### /pay

This command pays a user an amount of P-Coin specified. The source user must have the amount of P-Coin, as it will deduct the amount of P-Coin from the source user's pocket and add the amount to the target user's pocket.

This command must have 2 arguments passed. The **user** argument is the user to be paid, and the **amount** argument is the amount of P-Coin that should be paid to the user.

### /rob

This command robs a specific user's pocket. The source and target user must have at least 100 P-Coin.

There are 2 possible outcomes:
- The source user gets some of the target user's pocket
- The source user pays a fee and pays the target user some of their pocket

This command must have 1 argument passed. The **user** argument is the user to be robbed.

### /rps

This command creates a rock-paper-scissors game with another user using buttons, along with a specified bet of P-Coin. It is recommended you know how to play Rock Paper Scissors before running this command.

> **Note**: The bot can also play if you pass the bot itself in the **user** argument.

Upon running the command, an embed will be displayed prompting the target user to click a button. Once the button is pressed, a game starts with the source and target users.

The source and target user then press another button, with the labels `Rock`, `Paper`, and `Scissors`. The embed will edit prompting either of the users to click a button, which will then end the game or prompt the other user to click a button, depending on what had happened.

Once the game is finished, there are 3 possible outcomes.
- The source user wins, the target user pays the source user the bet
- The target user wins, the source user pays the target user the bet.
- The source and target user both tie, and nothing happens.

This command must have 2 arguments passed. The **user** argument is the user to challenge, and the **bet** argument is the amount of P-Coin to bet.

> **Note**: There can only be one game per channel. Ensure that no one else is playing a game in the channel before doing this.

### /withdraw

Using the **amount** argument, this command withdraws an amount of P-Coin from your vault into your pocket. (See **Vault** section)

This command must have 1 argument passed. The **amount** argument is how much you want to withdraw from your vault.