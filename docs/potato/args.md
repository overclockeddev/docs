---
sidebar_position: 4
---

# Arguments

Arguments are a key aspect into using Potato Bot.

Arguments are quite simple: type a command, then depending on your platform:

- Desktop: Press TAB until your desired argument is highlighted
- Mobile: Press the the desired argument

Please use arguments in the order they are already in. Using arguments in a different order will not work.