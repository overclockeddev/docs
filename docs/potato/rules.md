---
sidebar_position: 3
---
# Rules
By using Potato Bot, you agree to these rules.

## FAQ

### Why?
Potato Bot is meant to be a fun bot, but we need to set some ground rules to keep people from using Potato maliciously.

### What happens if I break them?
Depending on the severity of the rule that you broke, you may get a punishment spanning from a warning, to a P-Coin balance deduction, to a straight bot ban.

## The Rules
While these are here, we should take this time to say that our TOS is also in effect. Check that out in the Legal section of the sidebar.

- Your actions on Potato must follow the Discord TOS and Community Guidelines. For example, harrassing users using features on Potato is not permitted.
- You must not reward users P-Coin for joining your server.
- You must not "sell" access to Potato Bot in your server.
- Your server's purpose must not be to spam bots or the Discord API.
- You must not trade P-Coin for any real life items.
- You must not abuse bugs or exploits to gain an advantage.

## Keep in mind
- Our moderation team reserves the right to issue punishments for any (reasonable) reason.
- Our administration team reserves the right to issue punishments without a reason.