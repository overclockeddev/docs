---
sidebar_position: 6
---

# Music
Listen to music with Potato Bot!

## Important Information
Music has been removed to prevent Discord from unverifying our bot, as the music module uses YouTube.

### What happens next?
We are working on a replacement for music! It won't be the exact same, as Discord will allegedly just unverify the bot. Stay tuned!