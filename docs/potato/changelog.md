---
sidebar_position: 2
---

# Change Log

## Update 1ecd6de (December 19th, 2023)

- Added: /gamble again as Discord didn't seem to care, the chat AI model can now detect inappropriate topics and refuse to give a response, Potato Bot can now keep track of the last time you've run a command

- Changed: The chat AI model to be more up to date, and the chat response footer now links to OC's Privacy Policy

- Removed: /donate command, M-Coin system, and locking system

## Update fdb87572 (March 26th, 2023)

- Added: /chat and /choose commands, a filter for /8ball

- Changed: /meme now has a disclaimer at the bottom

- Removed: /gamble to comply with Discord's dumb app directory rules

## Update ab721ae7 (Feburary 7th, 2023)

- Added: /8ball and /moonbasetts commands

- Changed: Blackjack will now penalize you for waiting too long, an experiment was placed regarding vault increase rates
## Update f0ed279 (January 28th, 2023)

- Added: Feedback command (report bugs and give us suggestions)

- Fixed: An exploit that could be used to gain infinite P-Coin in minigames

- Changed: There is now a 1 billion P-Coin limit per wallet, vault increase rates have been tweaked

## Update 7b9c8428 (January 24th, 2023)

- Changed: Daily reward limit

- Refinements: Cleaned up some of the code to improve response times

- Removed: All music commands (for now)

## Update 6ed1d4b3 (November 10th, 2022)

- Added: Minigames

- Changed: Certain commands have been nerfed

- Fixed: A bug in which users were earning more vault space in an alarmingly fast rate, a bug in which developer broadcasts were not formatted properly

- Removed: Music

## Update f8e8bd61 (October 19th, 2022)

- Added: Developer Messages

- Internal: Cleaned up some of the code